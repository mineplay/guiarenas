package ru.mineplay.guiarenas;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import ru.mineplay.mineplayapi.game.match.Match;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class POJO {
	public int num;
	public OpenGui.Position position;
	public Match<?> match;

	public POJO(int n, OpenGui.Position pos, Match<?> m) {
		this.num = n;
		this.position = pos;
		this.match = m;
	}

	public static ArrayList<POJO> getAllFromPos(ArrayList<POJO> poj, OpenGui.Position pos) {
		ArrayList<POJO> ret = new ArrayList<>();
		poj.stream().filter(l -> l.comparatePos(pos)).forEach(ret::add);
		return ret;
	}

	public static ArrayList<POJO> sort(ArrayList<POJO> poj) {
		ArrayList<POJO> ret = new ArrayList<>();
		ret.addAll(poj.stream().filter(p -> p.position.equals(OpenGui.Position.GREEN) || p.position.equals(OpenGui.Position.ORANGE)).collect(Collectors.toList()));
		ret.addAll(poj.stream().filter(p -> p.position.equals(OpenGui.Position.RED)).collect(Collectors.toList()));
		return ret;
	}

	public static POJO getRandom(ArrayList<POJO> poj) {
		if (POJO.getAllFromPos(poj, OpenGui.Position.GREEN).size() != 0 || POJO.getAllFromPos(poj, OpenGui.Position.ORANGE).size() != 0)
			return poj.stream().filter(p -> p.position.equals(OpenGui.Position.GREEN) || p.position.equals(OpenGui.Position.ORANGE)).findAny().get();
		else
			return poj.stream().findAny().get();
	}

	public static POJO getFromArenaName(ArrayList<POJO> poj, String name) {
		AtomicReference<POJO> pojoret = new AtomicReference<>();
		poj.forEach(p -> {
			if (p.match.getArena().getName().equals(name)) pojoret.set(p);
		});
		return pojoret.get();
	}

	public ItemStack getIS() {
		if (position.equals(OpenGui.Position.GREEN))
			return new ItemStack(Material.STAINED_CLAY, 1, getWoolColor());
		if (position.equals(OpenGui.Position.ORANGE))
			return new ItemStack(Material.STAINED_CLAY, 1, getWoolColor());
		return new ItemStack(Material.STAINED_CLAY, 1, getWoolColor());
	}
	public boolean comparatePos(OpenGui.Position pos) {
		return pos.equals(position);
	}

	public short getWoolColor() {
		if (position.equals(OpenGui.Position.GREEN))
			return 0xD;
		else if (position.equals(OpenGui.Position.ORANGE))
			return 0x1;
		else
			return 0xE;
	}

	public List<String> getLore() {
		List<String> ret = new ArrayList<>();
		ret.add("Arena " + num);
		ret.add("Name: " + match.getArena().getName());
		ret.add(String.format("Players: %s/%s", match.getPlayers().size(), match.getMaxPlayers()));
		match.getPlayers().forEach(l -> ret.add(l.getName()));
		return ret;
	}
}

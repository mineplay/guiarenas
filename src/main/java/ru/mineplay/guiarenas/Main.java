package ru.mineplay.guiarenas;

import org.bukkit.plugin.java.JavaPlugin;
import ru.mineplay.mineplayapi.i18n.MPLocalizer;

public final class Main extends JavaPlugin {
	public static Main instance;
	@Override
	public void onEnable() {
		super.onEnable();
		MPLocalizer.loadLocaleFiles(this, "guiarenas");
		getCommand("mparenas").setExecutor(new CommandMPArenas());
		instance = this;
	}

	@Override
	public void onDisable() {
		// Plugin shutdown logic
	}
}

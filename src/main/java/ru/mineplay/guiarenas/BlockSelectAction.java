package ru.mineplay.guiarenas;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.mineplay.mineplayapi.game.match.CannotJoinException;
import ru.mineplay.mineplayapi.game.match.Match;
import ru.mineplay.mineplayapi.invgui.GuiElement;

import java.util.ArrayList;

import static ru.mineplay.mineplayapi.i18n.MPLocalizer.l6e;

public class BlockSelectAction implements GuiElement.Action {
	private ArrayList<POJO> pojos;

	public BlockSelectAction(ArrayList<POJO> pojos) {
		this.pojos = pojos;
	}

	@Override
	public boolean onClick(GuiElement.Click click) {
		if (!(click.getEvent().getWhoClicked() instanceof Player))
			return false;

		ItemStack stack = click.getElement().getItem(click.getSlot());
		Player player = (Player) click.getEvent().getWhoClicked();
		String stackname = stack.getItemMeta().getDisplayName();
		if (stackname.equals(l6e(player.getLocale(), "selectarena.random"))) {
			try {
				Match<?> match = Match.getMatch(player);
				if (match != null)
					match.leaveMatch(player, null);
				POJO.getRandom(pojos).match.joinMatch(player);
			} catch (CannotJoinException | NullPointerException e) {
				player.sendMessage(e.getMessage());
			}
			player.closeInventory();
			return true;
		}
		try {
			if (stackname.contains("Arena")) {
				String arenaname = stack.getItemMeta().getLore().get(0).split(" ")[1];
				Match<?> match = Match.getMatch(player);
				if (match != null)
					match.leaveMatch(player, null);
				POJO.getFromArenaName(pojos, arenaname).match.joinMatch(player);
				return true;
			}
		} catch (Exception e) {
			player.sendMessage(e.getMessage());
			e.printStackTrace();
		}

		player.closeInventory();
		return true;
	}
}

package ru.mineplay.guiarenas;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.mineplay.mineplayapi.game.MiniGame;
import ru.mineplay.mineplayapi.game.match.Match;
import ru.mineplay.mineplayapi.game.match.MatchRunState;
import ru.mineplay.mineplayapi.invgui.GuiElementGroup;
import ru.mineplay.mineplayapi.invgui.GuiPageElement;
import ru.mineplay.mineplayapi.invgui.InventoryGui;
import ru.mineplay.mineplayapi.invgui.StaticGuiElement;
import ru.mineplay.mineplayapi.server.ServerProvider;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import static ru.mineplay.mineplayapi.i18n.MPLocalizer.l6e;

public class OpenGui {
	private int pages = 1;
	/**
	 *  green wool	(<1/2 макс игроков на арене)
	 *  orange wool	(1/2 макс игроков на арене)
	 *  red wool		(арена забита полностью/арена запущена.)
	 *  white wool	(Неизвестность)
	 * r - star	(рандомный выбор свободной арены)
	 * n/p - black wool	(следущая/предыдущая страница)
	 */

	private ArrayList<POJO> arenas = new ArrayList<>();

	public OpenGui(Player p) {
		updateGuiLayout();
		openGuiСhoice(p);
	}

	private void updateGuiLayout() {
		try {
			MiniGame<?> minigame = Bukkit.getServer()
					.getServicesManager()
					.getRegistration(ServerProvider.class)
					.getProvider()
					.getMiniGame();
			if (minigame == null) {
				throw new NullPointerException("MiniGame == null");
			}
			pages = minigame.getArenas().size() / 18;
			addToArenas(minigame);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addToArenas(MiniGame<?> minigame) {
		AtomicInteger offset = new AtomicInteger();
		minigame.getArenas().forEach((s, a) -> {
			Match<?> match = a.getCurrentMatch();
			if (match.getRunState().getBaseState() == MatchRunState.RUNNING) {
				arenas.add(new POJO(offset.getAndIncrement(), Position.RED, match));
				return;
			}
			int max = match.getMaxPlayers();
			int size = match.getPlayers().size();

			if (max - size > max / 2)
				arenas.add(new POJO(offset.getAndIncrement(), Position.GREEN, match));
			else if (size != max)
				arenas.add(new POJO(offset.getAndIncrement(), Position.ORANGE, match));
			else
				arenas.add(new POJO(offset.getAndIncrement(), Position.RED, match));
		});
	}

	private void openGuiСhoice(Player pl) {
		InventoryGui gui = new InventoryGui(Main.instance, l6e(pl.getLocale(), "selectarena"),
				new String[]{"wwwwwwwww", "wwwwwwwww", "p   r   n"});
		gui.setFiller(new ItemStack(Material.AIR));

		gui.addElement(new GuiPageElement('n', new ItemStack(Material.WOOL, 1, (short) 0xF), GuiPageElement.PageAction.NEXT, l6e(pl.getLocale(), "selectarena.nextpage")));
		gui.addElement(new GuiPageElement('p', new ItemStack(Material.WOOL, 1, (short) 0xF), GuiPageElement.PageAction.PREVIOUS, l6e(pl.getLocale(), "selectarena.prevpage")));
		gui.addElement(new StaticGuiElement('r', new ItemStack(Material.NETHER_STAR), new BlockSelectAction(arenas), l6e(pl.getLocale(), "selectarena.random")));

		GuiElementGroup wool = new GuiElementGroup('w');
		for (POJO p : POJO.sort(arenas))
			wool.addElement(new StaticGuiElement('w', p.getIS(),
					new BlockSelectAction(arenas), p.getLore().toArray(new String[0])));

		gui.addElement(wool);
		gui.show(pl);
	}

	public enum Position {
		GREEN,
		ORANGE,
		RED
	}
}
